"""
Database functionality.
"""
import sqlite3


class Database:

    def __init__(self, filename="database.db"):
        self.filename = filename
        self.conn = None
        self.cursor = None
        self.open()
        self._create_tables()
        self.close()

    def open(self):
        if not self.conn:
            self.conn = sqlite3.connect(self.filename)
            self.cursor = self.conn.cursor()
        elif not self.cursor:
            self.cursor = self.conn.cursor()

    def close(self):
        if self.cursor:
            self.cursor.close()
        if self.conn:
            self.conn.commit()
            self.conn.close()
        self.conn = None
        self.cursor = None

    def _create_tables(self):
        with open("database.schema") as schema_file:
            schema = schema_file.read()
        self.cursor.executescript(schema)
        self.conn.commit()

    @staticmethod
    def _format_user(data):
        return {"email": data[0],
                "pass_hash": data[1],
                "salt": data[2],
                "first name": data[3],
                "family name": data[4],
                "gender": data[5],
                "city": data[6],
                "country": data[7],
                "private key": data[8]}

    def add_post(self, from_email, to_email, content):
        self.cursor.execute("INSERT INTO post (`from`, `to`, content) VALUES (?, ?, ?)", (from_email, to_email, content))
        self.conn.commit()

    def add_session(self, token, user):
        self.cursor.execute("INSERT INTO session (token, user) VALUES (?, ?)", (token, user))
        self.conn.commit()

    def add_user(self, email, pass_hash, salt, first_name, family_name, gender, city, country, private_key):
        self.cursor.execute("""
                        INSERT INTO 
                          user (email, password_hash, salt, first_name, family_name, gender, city, country, private_key) 
                       VALUES 
                          (?,?,?,?,?,?,?,?,?)
                      """,
                       (email, pass_hash, salt, first_name, family_name, gender, city, country, private_key))
        self.conn.commit()

    def get_user_by_email(self, email):
        self.cursor.execute(
            """
            SELECT 
              email, password_hash, salt, first_name, family_name, gender, city, country, private_key
            FROM 
              user 
            WHERE 
              email=?
            """, (email,))
        data = self.cursor.fetchone()
        if data:
            return self._format_user(data)

    def get_user_by_token(self, token):
        self.cursor.execute("""
                          SELECT 
                            email, password_hash, salt, first_name, family_name, gender, city, country, private_key 
                          FROM 
                            user, session 
                          WHERE 
                            user.email = session.user AND
                            session.token = ?
                      """, (token,))
        data = self.cursor.fetchone()
        if data:
            return self._format_user(data)
        return None

    def get_active_token(self, email):
        self.cursor.execute("SELECT token FROM session WHERE user=?", (email,))
        data = self.cursor.fetchone()
        if data:
            return data[0]
        return None

    def delete_token(self, token):
        self.cursor.execute("DELETE FROM session WHERE token=?", (token,))
        self.conn.commit()

    def get_posts_to_user(self, email):
        self.cursor.execute("SELECT `from`, content FROM post WHERE `to`=?", (email,))
        posts = self.cursor.fetchall()
        result = []
        for row in posts:
            result.append({"writer": row[0], "content": row[1]})
        return result

    def set_password(self, email, new_password_hash, salt):
        self.cursor.execute("UPDATE user SET password_hash=?, salt=? WHERE email=?", (new_password_hash, salt, email))
        self.conn.commit()

    def is_active_token(self, token):
        self.cursor.execute("SELECT COUNT(*) FROM session WHERE token=?", (token,))
        return self.cursor.fetchone()[0]

    def get_total_post_count(self):
        self.cursor.execute("SELECT COUNT(*) FROM post")
        return self.cursor.fetchone()[0]

    def get_posted_to(self, email):
        # Posts to yourself don't count
        self.cursor.execute("SELECT COUNT(*) FROM post WHERE `to`=? AND `from`!=?", (email, email))
        return self.cursor.fetchone()[0]

    def get_posted_by(self, email):
        # Posts to yourself don't count
        self.cursor.execute("SELECT COUNT(*) FROM post WHERE `from`=? AND `to`!=?", (email, email))
        return self.cursor.fetchone()[0]

    def get_register_users_count(self):
        self.cursor.execute("SELECT COUNT(*) FROM user")
        return self.cursor.fetchone()[0]

    def get_active_users_count(self):
        self.cursor.execute("SELECT COUNT(*) FROM session")
        return self.cursor.fetchone()[0]

    def get_email(self, token):
        self.cursor.execute("SELECT user FROM session WHERE token=?", (token,))
        return self.cursor.fetchone()[0]

    def delete_sessions(self):
        self.cursor.execute("DELETE FROM session")

    def clear_db(self):
        self.cursor.execute("DROP TABLE user")
        self.cursor.execute("DROP TABLE post")
        self.cursor.execute("DROP TABLE session")
        self._create_tables()
