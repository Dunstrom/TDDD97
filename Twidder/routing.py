"""
Routing the HTTP request to corresponding server methods.
"""
import json
from flask import Flask, request, jsonify
from flask_sockets import Sockets
from server import Server
from gevent import pywsgi
from geventwebsocket.handler import WebSocketHandler

app = Flask(__name__)
app.config['SECRET_KEY'] = "hemligt123"
server = Server()
sockets = Sockets(app)


def check_for_keys(keys, dict):
    """
    Makes sure that a list of keys exists in a dict.
    :param list keys: The list of keys.
    :param dict dict: The dictionary to check.
    :return: True if they exist false otherwise.
    """
    for key in keys:
        if key not in dict.keys():
            return False
    return True


def execute_post(data, keys, func):
    """
    Executes a post request using a function from the server and a list of keys identifying the arguments and their
    order for that function.
    :param dict data: The data from the request containing all the arguments for the function call.
    :param list keys: The list of keys that determine which part of the data should be used and in which order.
    :param func: The callable function to execute.
    :return: The result from the function func.
    """
    if not isinstance(data, dict) or not check_for_keys(keys, data):
        return False, jsonify({"success": False, "message": "Bad request"})
    args = [data[key] for key in keys]
    res = func(*args)
    return jsonify(res)


def handle_post(keys, func):
    """
    Handles a post request, validating the HMAC if necessary.
    :param list keys: The keys needed from the requests data.
    :param func: The function to use for executing the post request.
    :return: The result of the function func or an error message if the HMAC control fails.
    """
    data = json.loads(request.data)
    if 'hmac' in data:
        hmac = data.pop('hmac')
    else:
        hmac = ""
    if 'token' not in data:
        return execute_post(data, keys, func)
    elif server.control_hmac(data, data['token'], hmac):
        return execute_post(data, keys, func)
    else:
        return jsonify({"success": False, "message": "HMAC failure."})


def handle_get(args, token, hmac, func):
    if server.control_hmac(args, token, hmac):
        return jsonify(func(*args))
    else:
        return jsonify({"success": False, "message": "HMAC failure."})


@app.before_request
def set_up():
    server.before_request()


@app.after_request
def tear_down(response):
    server.after_request()
    return response


@app.route("/", methods=["GET"])
def index():
    return app.send_static_file("index.html")


@app.route("/sign_in", methods=["POST"])
def sign_in():
    return handle_post(['email', 'password'], server.sign_in)


@app.route("/sign_up", methods=["POST"])
def sign_up():
    return handle_post(['email', 'password', 'first_name', 'family_name', 'gender', 'city', 'country'],
                       server.sign_up)


@app.route("/sign_out", methods=["POST"])
def sign_out():
    return handle_post(['token'], server.sign_out)


@app.route("/change_password", methods=["POST"])
def change_password():
    return handle_post(['token', 'old_password', 'new_password'], server.change_password)


@app.route("/get_user_data_by_email/<string:email>/<string:token>/<string:hmac>", methods=["GET"])
def get_user_data_by_email(email, token, hmac):
    return handle_get([email, token], token, hmac, server.get_user_data_by_email)


@app.route("/get_user_data_by_token/<string:token>/<string:hmac>", methods=["GET"])
def get_user_data_by_token(token, hmac):
    return handle_get([token], token, hmac, server.get_user_data_by_token)


@app.route("/get_user_messages_by_token/<string:token>/<string:hmac>", methods=["GET"])
def get_user_messages_by_token(token, hmac):
    return handle_get([token], token, hmac, server.get_user_messages_by_token)


@app.route("/get_user_messages_by_email/<string:email>/<string:token>/<string:hmac>", methods=["GET"])
def get_user_messages_by_email(token, email, hmac):
    return handle_get([email, token], token, hmac, server.get_user_messages_by_email)


@app.route("/post_message", methods=["POST"])
def post_message():
    return handle_post(['token', 'message', 'email'], server.post_message)


@sockets.route('/ws')
def watch_session(web_socket):
    server.watch_web_socket(web_socket)
    return "Disconnected"


def main():
    flask_server = pywsgi.WSGIServer(('127.0.0.1', 8080), app, handler_class=WebSocketHandler)
    flask_server.serve_forever()


if __name__ == "__main__":
    main()
