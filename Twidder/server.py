"""
The server handling the backend logic layer
"""

import random
import string
import json
import hashlib
import uuid
import hmac
import geventwebsocket
from validate_email import validate_email
from database_helper import Database


class Server:
    """
    The class representing the server.
    """
    MIN_PASSWORD_LENGTH = 5

    def __init__(self, db_filename="database.db"):
        self.db = Database(db_filename)
        self.db.open()
        self.db.delete_sessions()
        self.db.close()
        self.web_socket_clients = {}

    @staticmethod
    def _generate_token(length):
        """
        Generates a random token of a specified length using ascii letters and numbers.
        :param int length: The requested length of the token.
        :return str: A randomly generated string of the requested length.
        """
        character_pool = string.ascii_letters + string.digits
        return ''.join(random.choice(character_pool) for i in range(length))

    @staticmethod
    def hash_password(password, salt=None):
        """
        Hashes a password either with a provided salt or a newly generated salt.
        :param str password: The password to hash.
        :param str salt: The salt to use when hashing.
        :return tuple: A tuple of the generated hash and used salt (hash, salt).
        """
        if not salt:
            salt = uuid.uuid4().hex
        return hashlib.sha512(password + salt).hexdigest(), salt

    @staticmethod
    def check_password(password, pass_hash, salt):
        """
        Controls a that a password matches a certain hash and salt.
        :param str password: The password to check.
        :param str pass_hash: The hash to check against
        :param str salt: The salt to use when generating the hash.
        :return bool: True if it matches false otherwise.
        """
        return pass_hash == Server.hash_password(password, salt)[0]

    @staticmethod
    def clean_user_data(user):
        """
        Removes secrets from the user data.
        :param dict user: The user data to clean.
        :return dict: The cleaned user data.
        """
        secret_keys = ["pass_hash", "salt", "private_key"]
        for key in secret_keys:
            if key in user:
                user.pop(key)
        return user

    def before_request(self):
        self.db.open()

    def after_request(self):
        self.db.close()

    def control_hmac(self, data, token, client_digest):
        """
        Controls a HMAC digest.
        :param object data: The data used to generate the digest.
        :param str token: The clients token used as a public key for the HMAC process.
        :param unicode client_digest: The HMAC digest from the client.
        :return: True if the client_digest is correct false otherwise.
        """
        user = self.db.get_user_by_token(token)
        if not user or "private key" not in user:
            return False
        key = user["private key"]
        body_digest = hmac.new(key, json.dumps(data, separators=(',', ':')), hashlib.sha256).hexdigest()
        return hmac.compare_digest(str(client_digest), body_digest)

    def sign_in(self, email, password):
        """
        Signs in the user after controlling the parameters.
        :param str email: The users email.
        :param str password: The users password.
        :return: If successful the token and private key are sent to the client. If the
        sign in fails a error message is returned.
        """
        if len(password) < self.MIN_PASSWORD_LENGTH:
            return {"success": False, "message": "Too short password."}
        user = self.db.get_user_by_email(email)
        if not user or not self.check_password(password, user["pass_hash"], user["salt"]):
            return {"success": False, "message": "Bad email or password."}
        new_token = self._generate_token(128)
        active_token = self.db.get_active_token(email)
        if active_token:
            self.sign_out(active_token)
        self.db.add_session(new_token, email)
        private_key = user['private key']
        self._ws_inform_active()
        return {"success": True, "message": "Successfully signed in.", "token": new_token, "private_key": private_key}

    def sign_up(self, email, password, first_name, family_name, gender, city, country):
        """
        Registers a uses by creating a account with some personal information. This information is controlled
        before creating the user. Informs all logged in users about the change in registered users upon success.
        :param str email: The users email must be unique for each user.
        :param str password: The password must be at least 5 characters long.
        :param str first_name: The users first name.
        :param str family_name: The users family name / last name / surname.
        :param str gender: The users gender, Male, Female or Other.
        :param str city: The users city of residence.
        :param str country: The users country of residence.
        :return dict: Success message or error message depending on outcome.
        """
        if not validate_email(email, check_mx=True):
            return {"success": False, "message": "Invalid email."}
        if len(password) < self.MIN_PASSWORD_LENGTH:
            return {"success": False, "message": "Password to short."}
        for field in [first_name, family_name, gender, city, country]:
            if not field:
                return {"success": False, "message": "Empty fields."}
        user = self.db.get_user_by_email(email)
        if user:
            return {"success": False, "message": "User already exists."}
        pass_hash, salt = self.hash_password(password)
        private_key = self._generate_token(128)
        self.db.add_user(email, pass_hash, salt, first_name, family_name, gender, city, country, private_key)
        self._ws_inform_registered()
        return {"success": True, "message": "Successfully created a new user."}

    def sign_out(self, token):
        """
        Signs out the user, removing it's token from the active sessions table in the database. Informs all
        active users about the change in active users upon success.
        :param str token: The token for the session to logout.
        :return dict: Success or error message depending on outcome.
        """
        user = self.db.get_user_by_token(token)
        if not user:
            return {"success": False, "message": "Bad token."}
        self.db.delete_token(token)
        if user["email"] in self.web_socket_clients:
            self._ws_send(user["email"], "sign out", True)
        self._ws_inform_active()
        return {"success": True, "message": "User signed out."}

    def change_password(self, token, old_password, new_password):
        """
        Changes the user with associated with the token if the parameters checks out.
        :param str token: The users session token.
        :param str old_password: The old password this is checked to be correct before changing password.
        :param str new_password: The new password this is checked to be of proper length before changing to it.
        :return dict: Success or error message depending on outcome.
        """
        if len(old_password) < self.MIN_PASSWORD_LENGTH or len(new_password) < self.MIN_PASSWORD_LENGTH:
            return {"success": False, "message": "Password too short."}
        if old_password == new_password:
            return {"success": False, "message": "New and old password must be different."}
        user = self.db.get_user_by_token(token)
        if not user:
            return {"success": False, "message": "Bad token."}
        if not self.check_password(old_password, user["pass_hash"], user["salt"]):
            return {"success": False, "message": "Wrong password."}
        new_pass_hash, salt = self.hash_password(new_password)
        self.db.set_password(user['email'], new_pass_hash, salt)
        return {"success": True, "message": "Password changed."}

    def get_user_data_by_token(self, token):
        """
        Returns the cleaned user data of the user associated with the token.
        :param str token: The token associated with the user which data you want.
        :return dict: Success or error message depending on outcome.
        """
        user = self.db.get_user_by_token(token)
        if not user:
            return {"success": False, "message": "Bad token."}
        user = self.clean_user_data(user)
        return {"success": True, "message": "User data retrieved.", "data": user}

    def get_user_data_by_email(self, email, token):
        if not self.db.is_active_token(token):
            return {"success": False, "message": "Bad token."}
        if not validate_email(email, check_mx=True):
            return {"success": False, "message": "Invalid email address."}
        user = self.db.get_user_by_email(email)
        if not user:
            return {"success": False, "message": "User don't exist."}
        user = self.clean_user_data(user)
        return {"success": True, "message": "User data retrieved.", "data": user}

    def get_user_messages_by_token(self, token):
        """
        Returns the user that is associated with a certain tokens messages (a.k.a posts). This is the messages
        posted to the user. Controls that the token is valid.
        :param str token: The users token whose messages you want.
        :return dict: A success message and the data if it works otherwise a error message.
        """
        user = self.db.get_user_by_token(token)
        if not user:
            return {"success": False, "message": "Bad token."}
        posts = self.db.get_posts_to_user(user["email"])
        return {"success": True, "message": "Retrieved posts.", "data": posts}

    def get_user_messages_by_email(self, email, token):
        """
        Returns the user that is associated with a certain emails messages (a.k.a posts). This is the messages
        posted to the user. Controls the email and token.
        :param str email: The users email for which you want to retrieve messages
        :param str token: The token for the user making the request.
        :return dict: A success message and the data if it works otherwise a error message.
        """
        if not self.db.is_active_token(token):
            return {"success": False, "message": "Bad token."}
        if not validate_email(email, check_mx=True):
            return {"success": False, "message": "Invalid email."}
        if not self.db.get_user_by_email(email):
            return {"success": False, "message": "User don't exist."}
        posts = self.db.get_posts_to_user(email)
        return {"success": True, "message": "Retrieved posts.", "data": posts}

    def post_message(self, token, message, email):
        """
        Posts a message from a user on another users wall after controlling that the parameters are fine.
        Informs the active clients with the new post count.
        :param str token: The token of the user posting the message.
        :param str message: The message to post.
        :param str email: The users email that you want to post the message to.
        :return dict: A success or error message.
        """
        if not message:
            return {"success": False, "message": "Empty message."}
        from_user = self.db.get_user_by_token(token)
        if not from_user:
            return {"success": False, "message": "Bad token."}
        if not validate_email(email, check_mx=True):
            return {"success": False, "message": "Invalid email."}
        to_user = self.db.get_user_by_email(email)
        if not to_user:
            return {"success": False, "message": "User don't exist."}
        self.db.add_post(from_user["email"], email, message)
        self._ws_inform_total_post_count()
        self._ws_inform_posted_to_count(email)
        self._ws_inform_posted_by_count(self.db.get_email(token))
        return {"success": True, "message": "Posted message."}

    def watch_web_socket(self, web_socket):
        """
        Watches a web socket connection, giving it some initial data and makes sure it don't close. Only logged in
        users make web socket connections.
        :param geventwebsocket.websocket.WebSocket web_socket: The web socket connection to the client.
        :return: None
        """
        # Get the clients email
        token = web_socket.receive()
        self.db.open()
        email = self.db.get_email(token)

        # Inform client about current state
        self.web_socket_clients[email] = web_socket
        self._ws_inform_active(email)
        self._ws_inform_total_post_count(email)
        self._ws_inform_registered(email)
        self._ws_inform_posted_by_count(email)
        self._ws_inform_posted_to_count(email)

        self.db.close()

        # Keep web socket alive so it can be used later when the stat change.
        while True:
            try:
                web_socket.receive()
            except geventwebsocket.WebSocketError:
                print "closed socket to" + email
                self.web_socket_clients.pop(email)
                return  # End the connection.

    def _ws_broadcast(self, key, value):
        """
        Broadcasts a key and a value to all connected clients over web sockets.
        :param str key: The name of the message to send used by the client to know what to do with the message.
        :param value: The data part of the message.
        :return: None
        """
        message = json.dumps({"message": key, key: value})
        to_remove = []
        for email, client in self.web_socket_clients.items():
            try:
                client.send(message)
            except geventwebsocket.WebSocketError:
                to_remove.append(email)
        for email in to_remove:
            print "closed socket to" + email
            self.web_socket_clients.pop(email)

    def _ws_send(self, email, key, value):
        """
        Sends a key and a value to a specific client identified by email over web sockets.
        :param email: The email of the client to send to.
        :param str key: The name of the message to send used by the client to know what to do with the message.
        :param value: The data part of the message.
        :return: None
        """
        message = json.dumps({"message": key, key: value})
        if email in self.web_socket_clients:
            try:
                self.web_socket_clients[email].send(message)
            except geventwebsocket.WebSocketError:
                print "closed socket to" + email
                self.web_socket_clients.pop(email)

    def _ws_inform_total_post_count(self, email=None):
        post_count = self.db.get_total_post_count()
        if not email:
            self._ws_broadcast("total_post_count", post_count)
        else:
            self._ws_send(email, "total_post_count", post_count)

    def _ws_inform_active(self, email=None):
        active = self.db.get_active_users_count()
        if not email:
            self._ws_broadcast("active", active)
        else:
            self._ws_send(email, "active", active)

    def _ws_inform_registered(self, email=None):
        registered = self.db.get_register_users_count()
        if not email:
            self._ws_broadcast("registered", registered)
        else:
            self._ws_send(email, "registered", registered)

    def _ws_inform_posted_by_count(self, email):
        post_count = self.db.get_posted_by(email)
        self._ws_send(email, "posted_by_count", post_count)

    def _ws_inform_posted_to_count(self, email):
        post_count = self.db.get_posted_to(email)
        self._ws_send(email, "posted_to_count", post_count)
