(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['browse'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                <span class=\""
    + alias4(((helper = (helper = helpers.searchMessageClass || (depth0 != null ? depth0.searchMessageClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"searchMessageClass","hash":{},"data":data}) : helper)))
    + " rounded mr-2 p-2\">"
    + alias4(((helper = (helper = helpers.searchMessage || (depth0 != null ? depth0.searchMessage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"searchMessage","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <section id=\"friend-page\" class=\"row mt-2\">\n            <div class=\"col-sm-4\">\n                <h3 id=\"user-name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\n                <div class=\"mb-2\">\n                    <span class=\"personal-info gender\">"
    + alias4(((helper = (helper = helpers.gender || (depth0 != null ? depth0.gender : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gender","hash":{},"data":data}) : helper)))
    + "</span><br>\n                    <span class=\"personal-info email\" id=\"friend-email\">"
    + alias4(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"email","hash":{},"data":data}) : helper)))
    + "</span><br>\n                    <span class=\"personal-info city\">"
    + alias4(((helper = (helper = helpers.city || (depth0 != null ? depth0.city : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"city","hash":{},"data":data}) : helper)))
    + "</span><br>\n                    <span class=\"personal-info country\">"
    + alias4(((helper = (helper = helpers.country || (depth0 != null ? depth0.country : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country","hash":{},"data":data}) : helper)))
    + "</span><br>\n                    <button type=\"button\" class=\"btn btn-outline-info\" id=\"update-wall-button\" onclick=\"return renderFriendPosts();\">Update wall</button>\n                </div>\n            </div>\n            <div class=\"col-8\">\n                <form action=\"#\" class=\"form-group\" onsubmit=\"return browsePost(event);\">\n                    <textarea ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\" id=\"friend-post-text\" class=\"form-control\" placeholder=\"What do you want to say to "
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "?\"></textarea><br>\n                    <input class=\"btn btn-outline-success\" type=\"submit\" value=\"Post\">\n                </form>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.postMessage : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </div>\n        </section>\n        <section id=\"posts\" class=\"row container-fluid mt-5\"></section>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                     <span class=\"rounded "
    + alias4(((helper = (helper = helpers.postMessageClass || (depth0 != null ? depth0.postMessageClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"postMessageClass","hash":{},"data":data}) : helper)))
    + " mr-2 p-2\">"
    + alias4(((helper = (helper = helpers.postMessage || (depth0 != null ? depth0.postMessage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"postMessage","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"container\" id=\"profile\">\n    <nav class=\"mb-5\">\n        <ul class=\"nav justify-content-center nav-tabs\">\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"home-menu\" href=\"#\" onclick=\"return activateTab(this);\">Home</a></li>\n            <li class=\"nav-item\"><a class=\"active nav-link\" id=\"browse-menu\" href=\"#\" onclick=\"return activateTab(this);\">Browse</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"account-menu\" href=\"#\" onclick=\"return activateTab(this);\">Account</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"data-menu\" href=\"#\" onclick=\"return activateTab(this);\">Data</a></li>\n        </ul>\n    </nav>\n    <section id=\"friend-search\" class=\"row\">\n        <form action=\"#\" novalidate onsubmit=\"return findFriend(event);\" class=\"form-inline col-12 justify-content-center\">\n            <label for=\"friend-search-email\" class=\"mr-2\">Find a friend</label>\n            <input class=\"form-control mr-2\" type=\"email\" id=\"friend-search-email\" placeholder=\"Enter a email\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.searchMessage : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <input class=\"btn btn-outline-primary\" type=\"submit\" value=\"Search\">\n        </form>\n    </section>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.friend : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});
})();