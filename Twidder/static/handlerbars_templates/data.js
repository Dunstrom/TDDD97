(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['data'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"container\" id=\"profile\">\n    <nav class=\"mb-5\">\n        <ul class=\"nav justify-content-center nav-tabs\">\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"home-menu\" href=\"#\" onclick=\"return activateTab(this);\">Home</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"browse-menu\" href=\"#\" onclick=\"return activateTab(this);\">Browse</a></li>\n            <li class=\"nav-item\"><a class=\" nav-link\" id=\"account-menu\" href=\"#\" onclick=\"return activateTab(this);\">Account</a></li>\n            <li class=\"nav-item\"><a class=\"active nav-link\" id=\"data-menu\" href=\"#\" onclick=\"return activateTab(this);\">Data</a></li>\n        </ul>\n    </nav>\n    <section class=\"row\">\n        <h3 class=\"title mb-4\">Twidder has a total of "
    + alias4(((helper = (helper = helpers.registeredUsers || (depth0 != null ? depth0.registeredUsers : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"registeredUsers","hash":{},"data":data}) : helper)))
    + " registered users.</h3>\n        <canvas id=\"users-chart\" class=\"col-12\"></canvas>\n        <h3 class=\"title mb-4 mt-4\">Twidder has a total of "
    + alias4(((helper = (helper = helpers.totalPostCount || (depth0 != null ? depth0.totalPostCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalPostCount","hash":{},"data":data}) : helper)))
    + " posts.</h3>\n        <canvas id=\"post-chart\" class=\"col-12\"></canvas>\n    </section>\n</div>";
},"useData":true});
})();