(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['home'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                    <span class=\"rounded "
    + alias4(((helper = (helper = helpers.postMessageClass || (depth0 != null ? depth0.postMessageClass : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"postMessageClass","hash":{},"data":data}) : helper)))
    + " mr-2 p-2\">"
    + alias4(((helper = (helper = helpers.postMessage || (depth0 != null ? depth0.postMessage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"postMessage","hash":{},"data":data}) : helper)))
    + "</span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"container\" id=\"profile\">\n    <nav class=\"mb-5\">\n        <ul class=\"nav justify-content-center nav-tabs\">\n            <li class=\"nav-item\"><a class=\"active nav-link\" id=\"home-menu\" href=\"#\" onclick=\"return activateTab(this);\">Home</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"browse-menu\" href=\"#\" onclick=\"return activateTab(this);\">Browse</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"account-menu\" href=\"#\" onclick=\"return activateTab(this);\">Account</a></li>\n            <li class=\"nav-item\"><a class=\"nav-link\" id=\"data-menu\" href=\"#\" onclick=\"return activateTab(this);\">Data</a></li>\n        </ul>\n    </nav>\n    <section class=\"row\">\n        <div class=\"col-sm-4\">\n            <h3 id=\"user-name\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h3>\n            <div class=\"mb-2\">\n                <span class=\"personal-info gender\">"
    + alias4(((helper = (helper = helpers.gender || (depth0 != null ? depth0.gender : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"gender","hash":{},"data":data}) : helper)))
    + "</span><br>\n                <span class=\"personal-info email\">"
    + alias4(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"email","hash":{},"data":data}) : helper)))
    + "</span><br>\n                <span class=\"personal-info city\">"
    + alias4(((helper = (helper = helpers.city || (depth0 != null ? depth0.city : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"city","hash":{},"data":data}) : helper)))
    + "</span><br>\n                <span class=\"personal-info country\">"
    + alias4(((helper = (helper = helpers.country || (depth0 != null ? depth0.country : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"country","hash":{},"data":data}) : helper)))
    + "</span><br>\n            </div>\n            <button type=\"button\" id=\"update-wall-button\" class=\"btn btn-outline-info\" onclick=\"return renderUserPosts();\">Update wall</button>\n        </div>\n        <div class=\"col-8\" id=\"post-box\">\n            <h4>Post a message</h4>\n            <form class=\"form-group\" action=\"#\" novalidate onsubmit=\"return homePost(event);\">\n                <div class=\"form-group\">\n                    <label for=\"post-to-email\">Who do you want to message?</label>\n                    <input class=\"form-control\" placeholder=\"Enter email\" type=\"email\" id=\"post-to-email\">\n                </div>\n                <textarea ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\" class=\"form-control\" id=\"post-text\" placeholder=\"What do you want to say...?\"></textarea><br>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.postMessage : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                <input class=\"btn btn-outline-success\" type=\"submit\" value=\"Post\">\n            </form>\n        </div>\n    </section>\n    <section class=\"row container-fluid mt-5\" id=\"posts\"></section>\n</div>";
},"useData":true});
})();