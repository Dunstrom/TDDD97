(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['welcome'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                <div class=\"alert-danger rounded p-2\" id=\"sign-in-error\"><p class=\"mb-0\">"
    + container.escapeExpression(((helper = (helper = helpers.signInError || (depth0 != null ? depth0.signInError : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"signInError","hash":{},"data":data}) : helper)))
    + "</p></div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper;

  return "                <div class=\"alert-danger rounded p-2\" id=\"signup-error\"><p class=\"mb-0\">"
    + container.escapeExpression(((helper = (helper = helpers.signUpError || (depth0 != null ? depth0.signUpError : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"signUpError","hash":{},"data":data}) : helper)))
    + "</p></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.lambda, alias3=container.escapeExpression;

  return "<div class=\"container\" id=\"welcome\">\n    <div class=\"row\">\n        <div class=\"col-md\">\n            <img src=\"static/images/wimage.png\" id=\"logo\">\n        </div>\n        <div class=\"col-md\" id=\"sign-in-up-form\">\n        <h4>Sign in</h4>\n        <form id=\"sign-in\" class=\"form-group\" action=\"#\" novalidate onsubmit=\"return signIn(event);\">\n            <div class=\"form-group\">\n                <label for=\"sign-in-email\">Email</label>\n                <input type=\"email\" class=\"form-control\" placeholder=\"Enter email\" name=\"sign-in-email\" id=\"sign-in-email\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"sign-in-password\">Password</label>\n                <input type=\"password\" class=\"form-control\" placeholder=\"Enter password\" name=\"sign-in-password\" id=\"sign-in-password\">\n            </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.signInError : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <input class=\"btn btn-outline-primary\" type=\"submit\" value=\"Sign in\">\n        </form>\n        <form id=\"sign-up\" class=\"form-group\" action=\"#\" novalidate onsubmit=\"return signUp(event);\">\n            <h4>Sign up</h4>\n            <div class=\"form-group\">\n                <label for=\"sign-up-email\">Email</label>\n                <input class=\"form-control-sm form-control\" type=\"email\" placeholder=\"Enter email\" name=\"sign-up-email\" id=\"sign-up-email\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.signUpData : depth0)) != null ? stack1.email : stack1), depth0))
    + "\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"sign-up-password\">Password</label>\n                <input class=\"form-control-sm form-control\" type=\"password\" placeholder=\"Enter Password\" name=\"sign-up-password\" id=\"sign-up-password\">\n                <input class=\"form-control-sm form-control\" type=\"password\" placeholder=\"Repeat password\" name=\"repeat-password\" id=\"repeat-password\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"first-name\">First name</label>\n                <input class=\"form-control-sm form-control\" type=\"text\" placeholder=\"Enter first name\" name=\"first-name\" id=\"first-name\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.signUpData : depth0)) != null ? stack1.first_name : stack1), depth0))
    + "\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"family-name\">Family name</label>\n                <input class=\"form-control-sm form-control\" type=\"text\" name=\"family-name\" placeholder=\"Enter family name\" id=\"family-name\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.signUpData : depth0)) != null ? stack1.family_name : stack1), depth0))
    + "\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"gender\">Gender</label>\n                <select name=\"gender\" id=\"gender\" class=\"form-control-sm form-control\">\n                    <option value=\"Male\">Male</option>\n                    <option value=\"Female\">Female</option>\n                    <option value=\"Other\">Other</option>\n                </select>\n            </div>\n            <div class=\"form-group\">\n                <label for=\"city\">City</label>\n                <input type=\"text\" class=\"form-control form-control-sm\" name=\"city\" id=\"city\" placeholder=\"Enter city\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.signUpData : depth0)) != null ? stack1.city : stack1), depth0))
    + "\">\n            </div>\n            <div class=\"form-group\">\n                <label for=\"country\">Country</label>\n                <input type=\"text\" class=\"form-control form-control-sm\" placeholder=\"Enter country\" name=\"country\" id=\"country\" value=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.signUpData : depth0)) != null ? stack1.country : stack1), depth0))
    + "\">\n            </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.signUpError : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            <input type=\"submit\" class=\"btn btn-outline-primary\" value=\"Sign up\">\n        </form>\n    </div>\n    </div>\n</div>";
},"useData":true});
})();