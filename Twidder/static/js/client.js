/**
 * Created by hampus on 2018-01-11.
 */

const MIN_PASSWORDS_LENGTH = 5;
const BASE_URL = "http://127.0.0.1:8080/";
let state = "welcome";
let friend = null;
let user = null;
let webSocket = null;
let activeUsers = 0;
let registeredUsers = 0;
let totalPostCount = 0;
let myPostCount = 0;
let myPostedCount = 0;

function isValidEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email !== '' && re.test(email);
}

function openWebSocket() {
    /**
     * Opens a web socket connection to the server and defining the different responses to the different messages.
     */

    if (localStorage.getItem('token') === null) {
        console.log("No token");
        return;
    }

    if (webSocket !== null) {
        console.log("Websocket exists");
        return;
    }

    console.log("opening socket");
    webSocket = new WebSocket("ws:127.0.0.1:8080/ws");

    webSocket.onopen = function () {
        webSocket.send(localStorage.getItem('token'));
    };

    webSocket.onerror = function (error) {
        console.log('WebSocket Error: ' + error);
    };

    webSocket.onmessage = function (ev) {
        const data = JSON.parse(ev.data);
        console.log(data.message);
        switch (data.message) {
            case "sign out":
                localStorage.removeItem('token');
                state = "welcome";
                friend = null;
                user = null;
                render();
                return;
            case "active":
                activeUsers = data.active;
                break;
            case "registered":
                registeredUsers = data.registered;
                break;
            case "total_post_count":
                totalPostCount = data.total_post_count;
                break;
            case "posted_by_count":
                myPostedCount = data.posted_by_count;
                break;
            case "posted_to_count":
                myPostCount = data.posted_to_count;
                break;
        }
        if (state === "data") {
            render();
        }
    };

    webSocket.onclose = function (ev) {
        console.log("Closed socket");
    }

}

function registerMustaschHelpers() {

    Handlebars.registerHelper('renderPosts', function(posts) {
        let out = "";
        posts.forEach(function (post, index) {
           out += "<div class='m-1 rounded p-2 border border-info post' draggable='true' ondragstart='drag(event)'><span class='font-weight-bold'>" + post.writer + "</span><br><p>" + post.content + "</p></div>"
        });
        return new Handlebars.SafeString(out);
    })

}

window.onload = function () {
    /**
     * Loads the logged in home view or welcome view depending on the client has a token or not.
     */
    registerMustaschHelpers();
    if (localStorage.getItem('token') === null) {
        state = "welcome"
    } else {
        state = "home";
        openWebSocket();
    }
    render();
};

function render(context = {}) {
    /**
     * Chooses a render method depending on the state the application are in.
     */
    switch (state) {
        case "home":
            renderHome(context);
            break;
        case "browse":
            renderBrowse(context);
            break;
        case "account":
            renderAccount(context);
            break;
        case "data":
            renderData(context);
            break;
        case "welcome":
        default:
            renderWelcome(context);
            break;
    }
}

function postJSON(method, data, on_succes, async=true) {
    /**
     * Generates a HMAC digest and sends a post request to the specified method with the data as a JSON string in the
     * body. If the request succeeds the on_success function is called.
     */
    const xhttp = new XMLHttpRequest();
    const key = sjcl.codec.utf8String.toBits(localStorage.getItem("private_key"));
    const out = (new sjcl.misc.hmac(key, sjcl.hash.sha256)).mac(JSON.stringify(data));
    const hmac = sjcl.codec.hex.fromBits(out);
    xhttp.open("POST", method, async);
    xhttp.setRequestHeader("Content-Type", "application/json");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            if (xhttp.status === 200) {
                return on_succes(JSON.parse(xhttp.responseText));
            }
        }
    };
    data.hmac = hmac;
    xhttp.send(JSON.stringify(data));
}

function getJSON(method, args, onSuccess) {
    /**
     * Generates a HMAC digest and sends a get request to the specified method with the arguments. If the request
     * succeeds the onSuccess function is called.
     */
    const xhttp = new XMLHttpRequest();
    const key = sjcl.codec.utf8String.toBits(localStorage.getItem("private_key"));
    const out = (new sjcl.misc.hmac(key, sjcl.hash.sha256)).mac(JSON.stringify(args));
    const hmac = sjcl.codec.hex.fromBits(out);
    args.push(hmac);
    args.forEach(function (value) { method += "/" + value });
    const url = BASE_URL + method;
    xhttp.open("GET", url, true);
    xhttp.onreadystatechange = function () {
      if (xhttp.readyState === 4) {
          if (xhttp.status === 200) {
              return onSuccess(JSON.parse(xhttp.responseText));
          }
      }
    };
    xhttp.send();
}

function renderUserPosts() {
    let context = {};
    getJSON("get_user_messages_by_token", [localStorage.getItem('token')], function (response) {
        if (response.success) {
           context.posts = response.data;
        } else {
           context.message = response.message;
        }
        let postsContainer = document.getElementById("posts");
        if (postsContainer) {
            postsContainer.innerHTML = Handlebars.templates.posts(context);
        }
    });
}

function renderFriendPosts() {
    let context = {};
    getJSON("get_user_messages_by_email", [friend.email, localStorage.getItem('token')], function (response) {
       if (response.success) {
           context.posts = response.data;
           document.getElementById("posts").innerHTML = Handlebars.templates.posts(context);
       } else {
           context.message = response.message;
           document.getElementById("posts").innerHTML = Handlebars.templates.posts(context);
       }
    });
}

function renderHome(context) {
    getJSON("get_user_data_by_token", [localStorage.getItem('token')], function (response) {
        if (response.success) {
            context.email = response.data.email;
            context.name = response.data["first name"] + " " + response.data["family name"];
            context.gender = response.data.gender;
            context.city = response.data.city;
            context.country = response.data.country;
            document.getElementById("app").innerHTML = Handlebars.templates.home(context);
            renderUserPosts();
        } else {
            context.message = response.message;
            document.getElementById("app").innerHTML = Handlebars.templates.home(context);
        }
    });
}

function renderData() {
    const context = {registeredUsers: registeredUsers, totalPostCount: totalPostCount};
    document.getElementById("app").innerHTML = Handlebars.templates.data(context);
    const userCanvas = document.getElementById("users-chart");
    const activeUsersPieChart = new Chart(userCanvas, {
        type: 'pie',
        data: {
            labels: ["active", "inactive"],
            datasets: [{
                data: [activeUsers, registeredUsers-activeUsers],
                backgroundColor: [
                    '#28a745',
                    '#dc3545',
                ],
                borderColor: [
                    '#28a745',
                    '#dc3545',
                ],
                borderWidth: 1
            }]
        }
    });
    const postCanvas = document.getElementById("post-chart");
    const postCountChart = new Chart(postCanvas, {
        type: 'bar',
        data: {
            labels: ["total", "to you", "from you"],
            datasets: [{
                label: "Your posts.",
                data: [myPostedCount + myPostCount, myPostCount, myPostedCount],
                backgroundColor: [
                    '#17a2b8',
                    '#43ef5a',
                    '#eded3b'
                ],
                borderColor: [
                    '#17a2b8',
                    '#43ef5a',
                    '#eded3b'
                ],
                borderWidth: 1
            }]
        }
    })
}

function renderWelcome(context) {
    document.getElementById("app").innerHTML = Handlebars.templates.welcome(context);
}

function renderBrowse(context) {
    if (friend !== null) {
        context.friend = true;
        context.name = friend["first name"] + " " + friend["family name"];
        context.email = friend.email;
        context.gender = friend.gender;
        context.country = friend.country;
        context.city = friend.city;
        document.getElementById("app").innerHTML = Handlebars.templates.browse(context);
        renderFriendPosts();
    } else {
        document.getElementById("app").innerHTML = Handlebars.templates.browse(context);
    }

}

function renderAccount(context) {
    document.getElementById("app").innerHTML = Handlebars.templates.account(context);
}

function signIn(event) {
    event.preventDefault();
    if (document.getElementById('sign-in-password').value.length < MIN_PASSWORDS_LENGTH) {
        return render({signInError: "Password not long enough"});
    }
    const email = document.getElementById('sign-in-email').value;
    if (!isValidEmail(email)) {
        return render({signInError: "Invalid email."})
    }
    const password = document.getElementById('sign-in-password').value;
    postJSON('sign_in', {email: email, password: password}, function (response) {
        if (response.success) {
            state = "home";
            localStorage.setItem('token', response.token);
            localStorage.setItem('private_key', response.private_key);
            openWebSocket();
            render();
        } else {
            state = "welcome";
            render({signInError: response.message});
        }
    });
    
}

function activateTab(menuItem) {

    switch (menuItem.id) {
        case "browse-menu":
            state = "browse";
            render();
            break;
        case "account-menu":
            state = "account";
            render();
            break;
        case "data-menu":
            state = "data";
            render();
            break;
        case "home-menu":
        default:
            state = "home";
            render();
    }
}

function signUp(event) {
    event.preventDefault();
    const user_email = document.getElementById('sign-up-email').value;
    const password = document.getElementById('sign-up-password').value;

    let signUpObject = {
        email: user_email,
        password: password,
        first_name: document.getElementById('first-name').value,
        family_name: document.getElementById('family-name').value,
        gender: document.getElementById('gender').value,
        city: document.getElementById('city').value,
        country: document.getElementById('country').value
    };

    let context = {signUpData: signUpObject};

    if (!validatePasswords(password, document.getElementById('repeat-password').value)) {
        context.signUpError = "The password need to match and be a minimum of 5 characters";
        return render(context);
    }
    if (!isValidEmail(user_email)) {
        context.signUpError = "Invalid email.";
        return render(context);
    }

    const values = [signUpObject.first_name, signUpObject.family_name, signUpObject.gender, signUpObject.city, signUpObject.country];
    let valuesFilled = true;
    values.forEach(function (value) { valuesFilled = valuesFilled && value !== "" });
    if (!valuesFilled) {
        context.signUpError = "Empty fields";
        return render(context);
    }

    postJSON("sign_up", signUpObject, function (response) {
        if (response.success) {
            state = "profile";
            postJSON("sign_in", {email: user_email, password: password}, function (response) {
                if (response.success) {
                    localStorage.setItem('token', response.token);
                    localStorage.setItem('private_key', response.private_key);
                    state = "home";
                    render();
                }
            });
        } else {
            context.signUpError = response.message;
            render(context);
        }
    });
    
}

function validatePasswords(password, repeat) {
    const same = password === repeat;
    return same && (password.length >= MIN_PASSWORDS_LENGTH);
}

function logout(event) {
    event.preventDefault();
    friend = null;
    user = null;
    postJSON("sign_out", {token: localStorage.getItem('token')}, function (response) {
        localStorage.removeItem('token');
        state = "welcome";
        render();    
    });
}

function changePassword(event) {
    event.preventDefault();
    let oldPassword = document.getElementById('old-password').value;
    let newPassword = document.getElementById('new-password').value;
    let repeatPassword = document.getElementById('repeat-password').value;
    let context;
    if (oldPassword === newPassword) {
        render({message: "New and old password can't be the same", messageClass: "alert-danger"});
    }
    else if (validatePasswords(newPassword, repeatPassword) && oldPassword.length >= MIN_PASSWORDS_LENGTH) {
        postJSON("change_password", {
            token: localStorage.getItem('token'),
            old_password: oldPassword,
            new_password: newPassword
        }, function (response) {
            context = {message: response.message};
            if (response.success) {
                context.messageClass = "alert-success";
            } else {
                context.messageClass = "alert-danger";
            }
            render(context);
        });
    } else {
        render({message: "New password and repeat password must match and password need to be at least 5 characters long.", messageClass: "alert-danger"});
    }
}

function post(content, toEmail) {

    if (!isValidEmail(toEmail)) {
        render({postMessage: "Invalid email", postMessageClass: "alert-danger"});
        return false;
    }

    let context = {};

    if (content.length > 0) {
        postJSON("post_message", {
            token: localStorage.getItem('token'),
            message: content,
            email: toEmail
        }, function (response) {
            context.postMessage = response.message;
            if (response.success) {
                context.postMessageClass = "alert-success";
                render(context);
                return true;
            } else {
                context.postMessageClass = "alert-danger";
                render(context);
                return false
            }

        });

    } else {
        context.postMessage = "Can't send a empty message";
        context.postMessageClass = "alert-danger";
        render(context);
        return false;
    }
}

function homePost(event) {
    event.preventDefault();
    const toInput = document.getElementById('post-to-email');
    const textInput = document.getElementById('post-text');
    const message = document.getElementById('post-message');
    if (post(textInput.value, toInput.value, message)) {
        toInput.value = "";
        textInput.value = "";
        renderUserPosts();
    }
}

function browsePost(event) {
    event.preventDefault();
    const toEmail = friend.email;
    const textInput = document.getElementById('friend-post-text');
    const message = document.getElementById('friend-post-message');
    if (post(textInput.value, toEmail, message)) {
        textInput.value = "";
        renderFriendPosts();
    }
}

function findFriend(event) {
    event.preventDefault();
    let friendSearch = document.getElementById('friend-search-email');
    if (!isValidEmail(friendSearch.value)) {
        return render({searchMessage: "Invalid email.", searchMessageClass: "alert-danger"});
    }
    getJSON("get_user_data_by_email", [friendSearch.value, localStorage.getItem('token')], function (response) {
        if (response.success) {
            friend = response.data;
            return render();
        } else {
            return render({searchMessage: response.message, searchMessageClass: "alert-danger"});
        }
    });

}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.children[2].innerText);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drop(ev) {
    ev.preventDefault();
    ev.target.value = ev.dataTransfer.getData("text");
}