"""
Testcases for testing the server.
"""
import unittest
import copy
from server import Server
from database_helper import Database

USER_PASS_1 = "12345"
PASS_HASH_1, SALT_1 = Server.hash_password(USER_PASS_1)
L_USER_1 = ["hampus@dunstrom.se", PASS_HASH_1, SALT_1, "Hampus", "Dunstrom", "Male", "Linkoping", "Sverige", "hemligt"]
D_USER_1 = {"email": L_USER_1[0],
            "pass_hash": L_USER_1[1],
            "salt": L_USER_1[2],
            "first name": L_USER_1[3],
            "family name": L_USER_1[4],
            "gender": L_USER_1[5],
            "city": L_USER_1[6],
            "country": L_USER_1[7],
            "private key": L_USER_1[8]}
USER_PASS_2 = "qwerty"
PASS_HASH_2, SALT_2 = Server.hash_password(USER_PASS_2)
L_USER_2 = ["max@dunstrom.se", PASS_HASH_2, SALT_2, "Max", "Dunstrom", "Male", "Linkoping", "Sverige", "hemligt2"]
D_USER_2 = {"email": L_USER_2[0],
            "pass_hash": L_USER_2[1],
            "salt": L_USER_2[2],
            "first name": L_USER_2[3],
            "family name": L_USER_2[4],
            "gender": L_USER_2[5],
            "city": L_USER_2[6],
            "country": L_USER_2[7],
            "private key": L_USER_2[8]}


def call_with_args_list(func, args):
    return func(*args)


class TestSignIn(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def test_correct(self):
        response = self.server.sign_in(L_USER_1[0], USER_PASS_1)
        self.assertEqual(response['success'], True, "No success")
        self.assertEqual(response['message'], "Successfully signed in.", "Message not ok")
        self.assertRegexpMatches(response['token'], r'[0-9a-zA-Z]{128}', "Token not ok")
        token = self.db.get_active_token(L_USER_1[0])
        self.assertNotEqual(token, None)
        self.assertEqual(token, response['token'], "Sent token no match for token in database.")

    def test_wrong_password(self):
        response = self.server.sign_in(L_USER_1[0], "123455")
        self.assertDictEqual(response, {"success": False, "message": "Bad email or password."})

    def test_short_password(self):
        response = self.server.sign_in(L_USER_1[0], "1234")
        self.assertDictEqual(response, {"success": False, "message": "Too short password."})

    def test_bad_email(self):
        response = self.server.sign_in("hampus@dnstrom.se", L_USER_1[1])
        self.assertDictEqual(response, {"success": False, "message": "Bad email or password."})

    def test_empty(self):
        response = self.server.sign_in("", "")
        self.assertEqual(response, {"success": False, "message": "Too short password."})

    def test_twice(self):
        response = self.server.sign_in(L_USER_1[0], USER_PASS_1)
        self.assertEqual(response['success'], True, "No success")
        self.assertEqual(response['message'], "Successfully signed in.", "Message not ok")
        self.assertRegexpMatches(response['token'], r'[0-9a-zA-Z]{128}', "Token not ok")
        first_token = self.db.get_active_token(L_USER_1[0])
        self.assertNotEqual(first_token, None)
        self.assertEqual(first_token, response['token'], "Sent token no match for token in database.")
        response = self.server.sign_in(L_USER_1[0], USER_PASS_1)
        self.assertEqual(response['success'], True, "No success")
        self.assertEqual(response['message'], "Successfully signed in.", "Message not ok")
        self.assertRegexpMatches(response['token'], r'[0-9a-zA-Z]{128}', "Token not ok")
        second_token = self.db.get_active_token(L_USER_1[0])
        self.assertNotEqual(second_token, None)
        self.assertEqual(second_token, response['token'], "Sent token no match for token in database.")
        self.assertNotEqual(first_token, second_token)


class TestSignUp(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def test_correct(self):
        self.db.clear_db()
        response = self.server.sign_up(D_USER_1["email"], USER_PASS_1, D_USER_1["first name"], D_USER_1["family name"],
                                       D_USER_1["gender"], D_USER_1["city"], D_USER_1["country"])
        expected_response = {"success": True, "message": "Successfully created a new user."}
        self.assertDictEqual(expected_response, response, "JSON response not correct")
        user = self.db.get_user_by_email(D_USER_1["email"])
        user = self.server.clean_user_data(user)
        expected_user = self.server.clean_user_data(D_USER_1)
        self.assertDictEqual(expected_user, user)

    def test_short_password(self):
        self.db.clear_db()
        response = self.server.sign_up(D_USER_1["email"], USER_PASS_1[0:4], D_USER_1["first name"],
                                       D_USER_1["family name"],
                                       D_USER_1["gender"], D_USER_1["city"], D_USER_1["country"])
        expected_response = {"success": False, "message": "Password to short."}
        self.assertDictEqual(expected_response, response, "Bad JSON response")
        user = self.db.get_user_by_email(L_USER_1[0])
        self.assertEqual(user, None, "User saved in database when it shouldn't be.")

    def test_bad_email(self):
        self.db.clear_db()
        response = self.server.sign_up("hampus.se", USER_PASS_1[0:4], D_USER_1["first name"],
                                       D_USER_1["family name"],
                                       D_USER_1["gender"], D_USER_1["city"], D_USER_1["country"])
        expected_response = {"success": False, "message": "Invalid email."}
        self.assertDictEqual(response, expected_response, "Bad JSON response")
        user = self.db.get_user_by_email(L_USER_1[0])
        self.assertEqual(user, None, "User saved in database when it shouldn't be.")

    def test_empty_fields(self):
        self.db.clear_db()
        response = self.server.sign_up("", "", "", "", "", "", "")
        expected_response = {"success": False, "message": "Invalid email."}
        self.assertDictEqual(expected_response, response)
        response = self.server.sign_up(L_USER_1[0], "", "", "", "", "", "")
        expected_response = {"success": False, "message": "Password to short."}
        self.assertDictEqual(expected_response, response)
        response = self.server.sign_up(L_USER_1[0], L_USER_1[1], "", "", "", "", "")
        expected_response = {"success": False, "message": "Empty fields."}
        self.assertDictEqual(expected_response, response)

    def test_existing_email(self):
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        response = self.server.sign_up(L_USER_1[0], L_USER_1[1], L_USER_1[2], L_USER_1[3], L_USER_1[4], L_USER_1[5], L_USER_1[6])
        expected_response = {"success": False, "message": "User already exists."}
        self.assertDictEqual(expected_response, response)


class TestSignOut(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        self.db.add_session("A" * 128, L_USER_1[0])
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def test_correct(self):
        response = self.server.sign_out("A"*128)
        expected_response = {"success": True, "message": "User signed out."}
        self.assertDictEqual(expected_response, response)
        token = self.db.get_active_token(L_USER_1[0])
        self.assertEqual(token, None, "Token not removed from database.")

    def test_bad_token(self):
        response = self.server.sign_out("B"*128)
        expected_response = {"success": False, "message": "Bad token."}
        self.assertDictEqual(expected_response, response)
        token = self.db.get_active_token(L_USER_1[0])
        self.assertEqual(token, "A"*128, "Token changed in database.")

    def test_no_token(self):
        response = self.server.sign_out("")
        expected_response = {"success": False, "message": "Bad token."}
        self.assertDictEqual(expected_response, response)
        token = self.db.get_active_token(L_USER_1[0])
        self.assertEqual(token, "A"*128, "Token changed in database.")


class TestChangePassword(unittest.TestCase):

    def _assert_password_changed_in_db(self, changed):
        user = self.db.get_user_by_email(L_USER_1[0])
        if not changed:
            self.assertEqual(user["pass_hash"], L_USER_1[1], "Password changed when it shouldn't be.")
        if changed:
            self.assertEqual(user["pass_hash"], self.server.hash_password(self.new_password, user["salt"])[0],
                             "Password not changed in db.")

    def _test_password(self, success, message, token, old_password, new_password):
        response = self.server.change_password(token, old_password, new_password)
        expected_response = {"success": success, "message": message}
        self.assertDictEqual(expected_response, response)
        self._assert_password_changed_in_db(success)

    def setUp(self):
        self.token = "A" * 128
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        self.db.add_session(self.token, L_USER_1[0])
        self.new_password = "qwerty"
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def test_correct(self):
        self._test_password(True, "Password changed.", self.token, USER_PASS_1, self.new_password)

    def test_bad_token(self):
        self._test_password(False, "Bad token.", "B" * 128, USER_PASS_1, self.new_password)

    def test_wrong_password(self):
        self._test_password(False, "Wrong password.", self.token, USER_PASS_1 + "lollipop", self.new_password)

    def test_short_passwords(self):
        self._test_password(False, "Password too short.", self.token, USER_PASS_1[1][:4], self.new_password)

    def test_empty(self):
        self._test_password(False, "Password too short.", self.token, USER_PASS_1[1], "")


class TestGetUserDataByToken(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        self.token = "A" * 128
        self.db.add_session(self.token, L_USER_1[0])
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def _test(self, success, message, token, data):
        response = self.server.get_user_data_by_token(token)
        if not data:
            expected_response = {"success": success, "message": message}
        else:
            expected_response = {"success": success, "message": message, "data": data}
        self.assertDictEqual(expected_response, response)

    def test_correct(self):
        user_data = copy.deepcopy(D_USER_1)
        user_data = self.server.clean_user_data(user_data)
        self._test(True, "User data retrieved.", self.token, user_data)

    def test_bad_token(self):
        self._test(False, "Bad token.", "D"*128, None)

    def test_empty(self):
        self._test(False, "Bad token.", "", None)


class TestGetUserDataByEmail(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        call_with_args_list(self.db.add_user, L_USER_2)
        self.token = "A" * 128
        self.db.add_session(self.token, L_USER_1[0])
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def _test(self, success, message, token, email, data):
        response = self.server.get_user_data_by_email(email, token)
        if not data:
            expected_response = {"success": success, "message": message}
        else:
            expected_response = {"success": success, "message": message, "data": data}
        self.assertDictEqual(expected_response, response)

    def test_correct(self):
        user_data = copy.deepcopy(D_USER_2)
        user_data = self.server.clean_user_data(user_data)
        self._test(True, "User data retrieved.", self.token, L_USER_2[0], user_data)

    def test_bad_token(self):
        self._test(False, "Bad token.", "D"*128, L_USER_2[0], None)

    def test_empty(self):
        self._test(False, "Bad token.", "", L_USER_2[0], None)

    def test_non_existing_user(self):
        self._test(False, "User don't exist.", self.token, "otto@dunstrom.se", None)

    def test_bad_email(self):
        self._test(False, "Invalid email address.", self.token, "otto@dunstrom", None)


class TestGetUserMessagesByToken(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        call_with_args_list(self.db.add_user, L_USER_2)
        self.token = "A" * 128
        self.db.add_session(self.token, L_USER_1[0])
        self.content = "Here we go again! :D"
        self.db.add_post(L_USER_2[0], L_USER_1[0], self.content)
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def _test(self, token, success, message, data):
        response = self.server.get_user_messages_by_token(token)
        if data:
            expected_result = {"success": success, "message": message, "data": data}
        else:
            expected_result = {"success": success, "message": message}
        self.assertDictEqual(expected_result, response)

    def test_correct(self):
        self._test(self.token, True, "Retrieved posts.", [{"writer": L_USER_2[0], "content": self.content}])

    def test_bad_token(self):
        self._test("B"*128, False, "Bad token.", None)

    def test_empty(self):
        self._test("", False, "Bad token.", None)


class TestGetUserMessagesByEmail(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        call_with_args_list(self.db.add_user, L_USER_2)
        self.token = "A" * 128
        self.db.add_session(self.token, L_USER_2[0])
        self.content = "Here we go again! :D"
        self.db.add_post(L_USER_2[0], L_USER_1[0], self.content)
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def _test(self, token, email, success, message, data):
        response = self.server.get_user_messages_by_email(email, token)
        if data:
            expected_result = {"success": success, "message": message, "data": data}
        else:
            expected_result = {"success": success, "message": message}
        self.assertDictEqual(expected_result, response)

    def test_correct(self):
        self._test(self.token, L_USER_1[0], True, "Retrieved posts.", [{"writer": L_USER_2[0], "content": self.content}])

    def test_bad_token(self):
        self._test("R"*128, L_USER_1[0], False, "Bad token.", None)

    def test_non_existing_user(self):
        self._test(self.token, "erik@dunstrom.se", False, "User don't exist.", None)

    def test_bad_email(self):
        self._test(self.token, "not@email", False, "Invalid email.", None)

    def test_empty(self):
        self._test(self.token, "", False, "Invalid email.", None)


class TestPostMessage(unittest.TestCase):

    def setUp(self):
        self.db = Database("test.db")
        self.db.open()
        self.db.clear_db()
        call_with_args_list(self.db.add_user, L_USER_1)
        call_with_args_list(self.db.add_user, L_USER_2)
        self.token = "A" * 128
        self.db.add_session(self.token, L_USER_2[0])
        self.content = "Here we go again! :D"
        self.server = Server("test.db")
        self.server.before_request()

    def tearDown(self):
        self.db.close()
        self.server.after_request()

    def _test(self, token, content, email, success, message):
        response = self.server.post_message(token, content, email)
        expected_result = {"success": success, "message": message}
        self.assertDictEqual(expected_result, response)
        posts = self.db.get_posts_to_user(email)
        if success:
            self.assertEqual(posts, [{"writer": L_USER_2[0], "content": self.content}], "Post not saved in database on success.")
        else:
            self.assertEqual(posts, [], "Post saved in database despite no success.")

    def test_correct(self):
        self._test(self.token, self.content, L_USER_1[0], True, "Posted message.")

    def test_bad_token(self):
        self._test("A"*127, self.content, L_USER_1[0], False, "Bad token.")

    def test_non_existing_user(self):
        self._test(self.token, self.content, "noexist@dunstrom.se", False, "User don't exist.")

    def test_bad_email(self):
        self._test(self.token, self.content, "non@valid", False, "Invalid email.")

    def test_empty(self):
        self._test(self.token, "", L_USER_1[0], False, "Empty message.")

    def test_post_to_yourself(self):
        self._test(self.token, self.content, L_USER_2[0], True, "Posted message.")


if __name__ == '__main__':
    unittest.main()
