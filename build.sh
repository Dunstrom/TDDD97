#!/usr/bin/env bash

echo "Creating virtual environment"
virtualenv -p python --clear .venv
source .venv/bin/activate
cd Twidder
pip install -r requirements.txt
python tests.py