#!/usr/bin/env bash

echo "Installing python"
apt install -y python python-pip

echo "Installing dependencies"
pip install -r requirements.txt

echo "Running tests"
python Twidder/tests.py